#!/bin/bash

#=============================================================================================
#
# Description:
#
#   Generate 'example' html using 'asciidoc' and 'asciidoctor'.
#
# Usage:
#
#   generate-html.sh asciidoc.adoc
#
# Copyright and License:
#
#   validatehl.sh
#   Copyright © 2022 RusSte, Inc.
#   License GPLv2: GNU GPL version 2 <https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>.
#   This is free software: you are free to change and redistribute it.
#   There is NO WARRANTY, to the extent permitted by law.
#
# Author:
#
#   Lawrence R. Steeger <russte.software@gmail.com>.
#
# --------------------------------------------------------------------------------------------
#
# Notes:
#   01 - Requires head, cut, tr
#   02 - Optionally requires asciidoc
#        Optionally requires asciidoctor
#
# Revisions (v1.1):
#   2022.04.29 - 1.1 Changed script prolog to RusSte, Inc. standard
#
# Revisions (v1.0):
#   2022.04.27 - 1.0 Created.
#
#=============================================================================================

set -u

declare -g scriptName="${0##*/}"
scriptName="${scriptName%*\.*}"
readonly scriptName

declare -- scriptNamePad=''
printf -v scriptNamePad ' %.0s' $( seq 1 ${#scriptName} )
readonly scriptNamePad

declare -r scriptVersion='1.1'

declare -r adocFilenameFull="${1?Example file w/extension}"
declare -r backend='html5'

if [[ ! -r "${adocFilenameFull}" ]]; then
  printf -- '`%s` does not exist!\n' "${adocFilenameFull}" 1>&2
  exit 1
fi

declare -- adocFilename="${adocFilenameFull%.*}"

declare -- execAsciidoc="$( command -v asciidoc )"
declare -- execAsciidoctor="$( command -v asciidoctor )"

if [[ -n "${execAsciidoc}" ]]; then
  declare -- versionAsciidoc="$( ${execAsciidoc} --version | head -1 | cut -d ' ' -f 2 | tr '.' '_' )"

  declare -a asciidocParams=()

  asciidocParams+=( '--verbose' )
  asciidocParams+=( '--backend='"${backend}" )
  asciidocParams+=( '--out-file='"${adocFilename}"'.ascidoc-'"${versionAsciidoc}"'.html' )
  asciidocParams+=( "${adocFilenameFull}" )

  echo "${scriptName}"':' "${execAsciidoc}" "${asciidocParams[@]}"
  "${execAsciidoc}" "${asciidocParams[@]}"
else
  printf -- 's: `asciidoc` is not available!\n' "${scriptName}" 1>&2
fi

if [[ -n "${execAsciidoctor}" ]]; then
  declare -- versionAsciidoctor="$( ${execAsciidoctor} --version | head -1 | cut -d ' ' -f 2 | tr '.' '_' )"

  declare -a asciidoctorParams=()

  asciidoctorParams+=( '--verbose' )
  asciidoctorParams+=( '--warnings' )
  asciidoctorParams+=( '--timings' )
  asciidoctorParams+=( '--backend='"${backend}" )
  asciidoctorParams+=( '--attribute=source-highlighter=highlight.js' )
  asciidoctorParams+=( '--out-file='"${adocFilename}"'.ascidoctor-'"${versionAsciidoctor}"'.html' )
  asciidoctorParams+=( "${adocFilenameFull}")

  echo "${scriptName}"':' "${execAsciidoctor}" "${asciidoctorParams[@]}"
  "${execAsciidoctor}" "${asciidoctorParams[@]}"
else
  printf -- '%s: `asciidoctor` is not available!\n'"${scriptName}" 1>&2
fi
