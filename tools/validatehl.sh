#!/bin/bash

#=============================================================================================
#
# Description:
#
#   Validates 'kate' highlighting xml.
#
# Usage:
#
#   validatehl.sh highlighting.xml
#
# Copyright and License:
#
#   validatehl.sh
#   Copyright © 2022 RusSte, Inc.
#   License GPLv2: GNU GPL version 2 <https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>.
#   This is free software: you are free to change and redistribute it.
#   There is NO WARRANTY, to the extent permitted by law.
#
# Author:
#
#   PRussell <russte.software@gmail.com>.
#
# --------------------------------------------------------------------------------------------
#
# Notes:
#   01 - Requires xmlint
#        Requires language.xsd
#
# Revisions (v1.1):
#   2022.04.29 - 1.1 Changed script prolog to RusSte, Inc. standard
#
# Revisions (v1.0):
#   2016.03.25 - 1.0 Created.
#
#=============================================================================================

set -u

declare -g scriptName="${0##*/}"
scriptName="${scriptName%*\.*}"
readonly scriptName

declare -- scriptNamePad=''
printf -v scriptNamePad ' %.0s' $( seq 1 ${#scriptName} )
readonly scriptNamePad

declare -r scriptVersion='1.1'

declare -r xml="${1:?Missing xml file}"
declare -r schema='language.xsd'

if [[ ! -e "${xml}" ]]; then
  printf -- '%s: xml file does not exist [%s]\n' "${scriptName}" "${xml}"
  exit 1
fi

if [[ ! -r "${xml}" ]]; then
  printf -- '%s: xml file is not readable by '"${USER}"' [%s]\n' "${scriptName}" "${xml}"
  exit 1
fi

if [[ ! -e "${schema}" ]]; then
  printf -- '%s: schema file does not exist [%s]\n' "${scriptName}" "${schema}"
  exit 1
fi

if [[ ! -r "${schema}" ]]; then
  printf -- '%s: schema file is not readable by '"${USER}"' [%s]\n' "${scriptName}" "${schema}"
  exit 1
fi

declare -a xmllintParams=()

xmllintParams+=( '--noout' )
xmllintParams+=( '--schema' )
xmllintParams+=( "${schema}" )
xmllintParams+=( "${xml}" )

echo "${scriptName}"':' xmllint "${xmllintParams[@]}"
xmllint "${xmllintParams[@]}"
