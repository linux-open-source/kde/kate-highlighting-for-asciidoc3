#!/bin/bash

#=============================================================================================
#
# Description:
#
#   View an asciidoc formatted file with highlighting at the console.
#
# Usage:
#
#   validatehl.sh highlighting.xml
#
# Copyright and License:
#
#   validatehl.sh
#   Copyright © 2022 RusSte, Inc.
#   License GPLv2: GNU GPL version 2 <https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>.
#   This is free software: you are free to change and redistribute it.
#   There is NO WARRANTY, to the extent permitted by law.
#
# Author:
#
#   PRussell <russte.software@gmail.com>.
#
# --------------------------------------------------------------------------------------------
#
# Notes:
#   01 - Requires source-highlight:
#
#          See: https://www.gnu.org/software/src-highlite/source-highlight.html
#
#   02 - Requires less
#
#   03 - Requires support files:
#
#          BlockTerminator.lang
#          InlineSubstitution.lang
#          asciidoc.lang
#          asciidoc.outlang
#          asciidoc.style
#          lang.map
#          outlang.map
#          style.defaults
#
# Revisions (v1.1):
#   2022.04.29 - 1.1 Changed script prolog to RusSte, Inc. standard
#
# Revisions (v1.0):
#   2016.03.25 - 1.0 Created.
#
#=============================================================================================

set -u

declare -g scriptName="${0##*/}"
scriptName="${scriptName%*\.*}"
readonly scriptName

declare -- scriptNamePad=''
printf -v scriptNamePad ' %.0s' $( seq 1 ${#scriptName} )
readonly scriptNamePad

declare -r scriptVersion='1.1'

declare -r  inFile="${1:-../examples/example01.adoc}"
declare -r  dataDir='./source-highlight'
declare -r  lang='asciidoc'
declare -r  outlang='asciidoc'
declare -r  style='asciidoc.style'
declare -a  shlParams=()
declare -a  lessParams=()

shlParams+=( '--data-dir='"${dataDir}" )
shlParams+=( '--lang-def='"${lang}.lang" )
shlParams+=( '--out-format='"${outlang}" )
shlParams+=( '--style-file='"${style}" )
shlParams+=( '--input='"${inFile}" )

lessParams+=( '--LINE-NUMBERS' )        # -N
lessParams+=( '--status-column' )       # -J
lessParams+=( '--ignore-case' )         # -i
lessParams+=( '--RAW-CONTROL-CHARS' )   # -R

echo "${scriptName}"':' source-highlight "${shlParams[@]}" \| less "${lessParams[@]}"
source-highlight "${shlParams[@]}" | less "${lessParams[@]}"

# ------------------------------------------------------------------------------
# Check everything is okay with source-highlight setup.
# ------------------------------------------------------------------------------
function check_setup() {
  source-highlight  --check-lang "${dataDir}/${lang}.lang"
  printf -- '\n-------------------\n'
  source-highlight  --check-outlang "${dataDir}/${lang}.outlang"
  printf -- '\n-------------------\n'
  source-highlight  --verbose --data-dir="${dataDir}" --lang-list
  printf -- '\n-------------------\n'
  source-highlight  --verbose --data-dir="${dataDir}" --outlang-list
  printf -- '\n-------------------\n'
  source-highlight  --verbose --data-dir="${dataDir}" --show-lang-elements="${lang}.lang"
  printf -- '\n-------------------\n'
  source-highlight  --verbose --data-dir="${dataDir}" --show-regex="${lang}.lang"

  return 0
}
